package main

import "github.com/muesli/termenv"

var (
	// variables -- general
	profile      = termenv.ColorProfile()
	wordwrap_len = 80

	// variables -- typesetting(ish)
	preHeader      = "\n"
	postHeader     = "\n"
	preWordType    = " "
	postWordType   = " "
	preDefinition  = ""
	postDefinition = "\n"
	noWordType     = "- " // used if wordType is empty

	// styles -- ui
	styleHelp       = newStyle(profile.Color("none"), profile.Color("11"), false, true)
	styleHighlight  = newStyle(profile.Color("15"), profile.Color("13"), true, false)
	stylePercentage = newStyle(profile.Color("none"), profile.Color("none"), false, true)

	// styles -- render
	styleHeading    = newStyle(profile.Color("2"), profile.Color("none"), true, false)
	styleWordType   = newStyle(profile.Color("none"), profile.Color("none"), false, true)
	styleDefinition = newColorStyle(profile.Color("none"), profile.Color("none"))
)

// easier to read
type styleFunction func(string) string

// Returns a termenv style from arguments
func newStyle(foreground, background termenv.Color, bold, faint bool) styleFunction {
	style := termenv.Style{}.Foreground(foreground).Background(background)

	if bold {
		style = style.Bold()
	}

	if faint {
		style = style.Faint()
	}

	return style.Styled
}

// Returns termenv style from only background and foreground colors
func newColorStyle(foreground, background termenv.Color) styleFunction {
	return newStyle(foreground, background, false, false)
}
