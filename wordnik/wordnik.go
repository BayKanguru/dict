package wordnik

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"codeberg.org/baykanguru/dict/lib"
	"github.com/PuerkitoBio/goquery"
)

func Query(query string) map[string][]lib.Definition {
	client := &http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("https://wordnik.com/words/%s", query), nil)
	if err != nil {
		log.Fatalf("couldn't make request: %s", err)
	}
	req.Header.Set("user-agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")

	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("couldn't get response: %s", err)
	}

	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	defs := make(map[string][]lib.Definition)
	s := doc.Find(".guts").First()
	s.Find("h3.source").Each(func(i int, s *goquery.Selection) {
		deff := []lib.Definition{}
		s.Next().Children().Each(func(i int, s *goquery.Selection) {

			deff = append(deff, lib.Definition{
				WordType: strings.TrimSpace(s.Find("[title = 'partOfSpeech']").Text()),
				Content: strings.TrimSpace(
					strings.TrimPrefix(s.Text(), s.Find("[title = 'partOfSpeech']").Text())),
			})

		})
		defs[s.Text()] = deff
	})
	return defs
}
