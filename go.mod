module codeberg.org/baykanguru/dict

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/antchfx/xmlquery v1.3.9
	github.com/charmbracelet/bubbles v0.9.0
	github.com/charmbracelet/bubbletea v0.19.2
	github.com/fluhus/gostuff v0.1.9
	github.com/muesli/ansi v0.0.0-20211031195517-c9f0611b6c70
	github.com/muesli/reflow v0.3.0
	github.com/muesli/termenv v0.9.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/antchfx/xpath v1.2.0 // indirect
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/charmbracelet/lipgloss v0.4.0 // indirect
	github.com/containerd/console v1.0.3 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.6 // indirect
)
