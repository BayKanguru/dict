package main

import (
	"strings"

	"codeberg.org/baykanguru/dict/lib"
	"github.com/muesli/reflow/indent"
	"github.com/muesli/reflow/wordwrap"
)

func formatContent(content map[string][]lib.Definition) string {
	var output strings.Builder
	for key, definitions := range content {

		output.WriteString(preHeader + styleHeading(key) + postHeader)

		for _, definition := range definitions {
			if definition.WordType != "" {
				output.WriteString(preWordType +
					styleWordType(definition.WordType) +
					postWordType +
					preDefinition +
					styleDefinition(definition.Content) +
					postDefinition)
			} else {
				output.WriteString(preWordType +
					styleWordType(noWordType) +
					styleDefinition(definition.Content) +
					postDefinition)
			}
		}
	}
	return indent.String(wordwrap.String(output.String(), wordwrap_len), 4)
}
