# dict

![Video of dict working in a terminal.](./media/dict.gif)

An English dictionary for commandline.
It uses [charm](https://charm.sh) for it's TUI.
Data comes only from [wordnik](https://wordnik.com/) for now.
In the dev branch I'm working on local [GCIDE](https://gcide.gnu.org.ua/) and [WordNet](https://wordnet.princeton.edu/) support.

## To Do

- Local dictionary support (GCIDE, WordNet)
- Asynchronised dictionary queries and lazyloading dictionary content
- Add "help" for the viewport
