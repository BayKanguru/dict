package lib

// definitions that are used all around the program

type Definition struct {
	WordType string
	Content  string
}
