package main

import (
	"log"

	tea "github.com/charmbracelet/bubbletea"
)

func main() {
	p := tea.NewProgram(initialModel("search"), tea.WithAltScreen())

	if err := p.Start(); err != nil {
		log.Fatal(err)
	}

}
