package main

import (
	"fmt"
	"math"
	"strings"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/muesli/ansi"

	"codeberg.org/baykanguru/dict/wordnik"
)

const (
	headerHeight = 0
	footerHeight = 1
)

var (
	viewportHelpHeight int
)

// KeyMap definition
type KeyMap struct {
	Enter key.Binding
	Help  key.Binding
	Quit  key.Binding
}

func (k KeyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Quit, k.Help}
}

func (k KeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.Enter, k.Quit, k.Help},
	}
}

var DefaultKeyMap = KeyMap{
	Quit: key.NewBinding(
		key.WithKeys("ctrl+c", "esc"),       // actual keybindings
		key.WithHelp("ctrl+c/esc", "Quit "), // corresponding help text
	),
	Enter: key.NewBinding(
		key.WithKeys("enter"),
		key.WithHelp("enter", "Send "),
	),
	Help: key.NewBinding(
		key.WithKeys("?"),
		key.WithHelp("?", "Help "),
	),
}

// model definition
type model struct {
	textInput    textinput.Model
	currentQuery string
	state        string
	ready        bool

	content          string
	viewport         viewport.Model
	showViewportHelp bool

	width  int
	height int

	help     help.Model
	keys     KeyMap
	quitting bool
}

type newQueryMsg struct{}

type changeStateMsg struct {
	newState string
}

func changeState(state string) tea.Cmd {
	return func() tea.Msg {
		return changeStateMsg{
			newState: state,
		}
	}
}

func newQuery() tea.Cmd {
	return func() tea.Msg {
		return newQueryMsg{}
	}
}

func initialModel(state string) model {
	ti := textinput.NewModel()
	ti.Placeholder = "Enter the query"
	ti.Focus()
	ti.CharLimit = 156
	ti.Width = 20

	return model{
		textInput: ti,
		state:     state,
		keys:      DefaultKeyMap,
	}
}

func (m model) Init() tea.Cmd {
	cmds := []tea.Cmd{
		textinput.Blink,
	}

	return tea.Batch(cmds...)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		cmd  tea.Cmd
		cmds []tea.Cmd
	)

	switch msg := msg.(type) {
	case newQueryMsg:
		m.currentQuery = m.textInput.Value()
		m.content = formatContent(wordnik.Query(m.currentQuery))
		m.viewport.SetContent(m.content)
		m.ready = true
		return m, nil

	case changeStateMsg:
		m.state = msg.newState

	case tea.WindowSizeMsg:
		// for viewport
		verticalMargins := headerHeight + footerHeight
		if !m.ready && m.content != "" {
			m.viewport = viewport.Model{Width: msg.Width, Height: msg.Height - verticalMargins}

			m.viewport.YPosition = headerHeight + 1
		} else {
			m.viewport.Width = msg.Width
			m.viewport.Height = msg.Height - verticalMargins
		}

		// if m.showViewportHelp {
		// 	if viewportHelpHeight == 0 {
		// 		viewportHelpHeight = strings.Count(m.helpView(), "\n")
		// 	}
		// 	m.viewport.Height -= (footerHeight + viewportHelpHeight)
		// }

		m.width = msg.Width
		m.height = msg.Height

		m.help.Width = m.width

	case tea.KeyMsg:
		switch {

		case key.Matches(msg, m.keys.Quit):
			m.quitting = true
			return m, tea.Quit

		case key.Matches(msg, m.keys.Enter):
			return m.handleSubmission()

		case key.Matches(msg, m.keys.Help):
			m.help.ShowAll = !m.help.ShowAll

		default:
			var inputCmd tea.Cmd
			m.textInput, inputCmd = m.textInput.Update(msg)
			cmds = append(cmds, inputCmd)
		}
	}

	m.viewport, cmd = m.viewport.Update(msg)
	cmds = append(cmds, cmd)

	return m, tea.Batch(cmds...)
}

func (m model) View() string {
	var b strings.Builder

	helpView := m.help.View(m.keys)

	switch m.state {
	case "display":
		if !m.ready {
			return "\n  Initializing..."
		}
		fmt.Fprint(&b, m.viewport.View()+"\n")
		fmt.Fprint(&b, m.bottomBar())
		// if m.showViewportHelp {
		// 	fmt.Fprint(&b, m.viewport)
		// }

	default:
		fmt.Fprint(&b, m.textInput.View()+"\n\n")
		fmt.Fprint(&b, helpView)
	}

	return b.String()
}

// parts of viewport

func (m model) bottomBar() string {
	const (
		minPercent               float64 = 0.0
		maxPercent               float64 = 1.0
		percentToStringMagnitude float64 = 100.0
	)

	queryWord := styleHighlight(" " + m.currentQuery + " ")

	percent := math.Max(minPercent, math.Min(maxPercent, m.viewport.ScrollPercent()))
	scrollPercent := stylePercentage(fmt.Sprintf(" %3.f%% ", percent*percentToStringMagnitude))

	// max to make padding at least 0 wide for negative values
	paddingSize := max(0,
		m.width-
			ansi.PrintableRuneWidth(queryWord)-
			ansi.PrintableRuneWidth(scrollPercent))
	padding := strings.Repeat(" ", paddingSize)

	return fmt.Sprintf("%s%s%s", queryWord, padding, scrollPercent)
}

// other functions

func (m model) handleSubmission() (model, tea.Cmd) {
	cmds := []tea.Cmd{
		changeState("display"),
		newQuery(),
	}

	return m, tea.Batch(cmds...)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
